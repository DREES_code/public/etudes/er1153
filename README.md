# ER1153

Ce dossier fournit les programmes permettant de produire les illustrations de l'Études et résultats n° 1153 de la DREES : "Allocation personnalisée d’autonomie : en 2017, un bénéficiaire sur deux n’utilise pas l’intégralité du montant d’aide humaine notifié".

Le fichier "ER.R" produit les tableaux issus de la base anonymisée, privée de la variable TYPO et SERVICE.
Le fichier "ER_CASD.R" produit quant à lui les tableaux et illustrations issus de la base CASD.

Le dossier "Tableeaux de résultats" contient des tableaux de références obtenus a partir des bases CASD et Anonymisée.

Lien vers l'étude:https://drees.solidarites-sante.gouv.fr/publications/etudes-et-resultats/allocation-personnalisee-dautonomie-en-2017-un-beneficiaire-sur) Grand âge & autonomie. 
	
Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : [enquête  RI APA 2017](https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/01-les-donnees-individuelles-sur-lallocation-personnalisee-dautonomie#:~:text=Les%20remont%C3%A9es%20individuelles%20APA%20de%202007&text=Les%20donn%C3%A9es%2C%20anonymis%C3%A9es%2C%20concernent%20plus,du%20dispositif%20de%20l'APA) (Drees). Traitements : Drees.


Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 3.6.3 , le 31/05/2021.
